{-# LANGUAGE OverloadedStrings #-}
module Article where
import Data.Text.Lazy (Text)
import Data.Aeson

-- Article constructor: Article 12 "some title" "some body text"
data Article = Article Integer Text Text
  deriving (Show)

-- Article from json instructions
instance FromJSON Article where
  parseJSON (Object v) = Article <$>
                         v .:? "id" .!= 0 <*> -- optional field denoted by "?"
                         v .:  "title"    <*>
                         v .:  "bodyText"

-- Article to json instructions
instance ToJSON Article where
  toJSON (Article id title bodyText) =
    object ["id" .= id,
            "title" .= title,
            "bodyText" .= bodyText]
