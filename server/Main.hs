{-# LANGUAGE OverloadedStrings #-}
import Web.Scotty
import Article

main :: IO ()
main = scotty 3000 $ do
  get "/article" $ do -- `http get :3000/article`
    json $ Article 13 "caption" "content" -- calls Article ToJSON instance (encode)
  
  post "/article" $ do -- `http post :3000/article id:=23 title="new caption" bodyText="some content"`
    article <- jsonData :: ActionM Article -- calls Article FromJSON instance (decode)
    json article

  -- get "/" $ do
  --   text "This was a GET request!"
  -- delete "/" $ do
  --   html "This was a DELETE request!"
  -- post "/" $ do
  --   text "This was a POST request!"
  -- put "/" $ do
  --   text "This was a PUT request!"
  
  -- post "/set-headers" $ do
  --   status status302 -- Respond with HTTP 302 status code
  --   setHeader "Location" "http://www.duckduckgo.com"
  -- get "/askfor/:word" $ do
  --   w <- param "word"
  --   html $ mconcat ["<h1>You asked for ", w, ", you got it!</h1>"]
  -- post "/submit" $ do -- `http post :3000/submit?name=helena
  --   name <- param "name"
  --   text name
  -- matchAny "/all" $ do
  --   text "matches all methods"
  -- notFound $ do
  --   text "there is no such route."