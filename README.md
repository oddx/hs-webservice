# hs-webservice
Simple haskell webservice to encode and decode `Article`s as json

## Installation
If you do not have haskell tooling set up on your machine, please see the [Opinionated Guide to Getting Started with Haskell](https://wasp-lang.dev/blog/2022/09/02/how-to-get-started-with-haskell-in-2022)
Otherwise, simply run the following:
```
git clone https://gitlab.com/oddx/hs-webservice.git
cd hs-webservice
cabal run
```
To interact with the webservice:

`http get :3000/article` to get an article

`http post :3000/article id:=23 title="new caption" bodyText="some content"` to post an article

## About
The service was created following [this](https://dev.to/parambirs/how-to-write-a-dockerized-haskell-web-servicefrom-scratch---part-1-3m7c) guide and was built on [Scotty](https://github.com/scotty-web/scotty), a sinatra like web server, and [Aeson](https://github.com/haskell/aeson), haskell's legendary json parsing library

Thanks for checking it out!